
PAGES = fe onglets \
	diu-eil capes isn \
	contact

portail/%.php : src/%.md
	pandoc $< -o $@
portail/%.php : src/%.php
	cp -af $< $@

# install -> ./portail -> git 
.PHONY : install
install : $(addprefix portail/,$(addsuffix .php,$(PAGES)))
	git status 
	@echo "\n\t→→ git commit, git push pour mettre en ligne sur le portail"
