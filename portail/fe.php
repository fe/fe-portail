<h2 id="formation-des-enseignants">Formation des enseignants</h2>
<p>L’informatique, ou de l’informatique, est aujourd’hui enseignée de l’école au lycée.</p>
<p>Le département informatique, en partenariat avec les autres départements informatiques de l’université — IUT, Polytech, etc. — assure la formation continue, bientôt initiale, d’enseignants du primaire et du secondaire à l’informatique.</p>
<ul>
<li>nous avons assuré la formation des enseignants de la spécialité ISN, <em>Informatique et science du numérique</em>, de l’académie de Lille en 2011-13. Puis participé ponctuellement à un nouveau cycle de formation en 2018.</li>
<li>nous avons mis en place une formation <em>Algorithmique et programmation au lycée — Python</em> à destination des enseignants de mathématiques à l’occasion de l’aménagement du programme de maths de la classe de seconde entré en vigueur à la rentrée 2017.</li>
<li>nous proposons une journée annuelle <em>JEIA</em> à destination de toutes celles et ceux qui enseignent de l’informatique, de l’école à l’université.</li>
<li>nous proposons le DIU <em>Enseigner l’informatique au lycée</em> pour formation des futurs enseignants de la spécialité NSI, <em>Numérique et sciences informatiques</em>.</li>
<li>nous travaillons à la mise en place d’une prépa CAPES informatique qui ouvrira dès la rentére 2019.</li>
<li>…</li>
</ul>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/fe/">portail.fil.univ-lille1.fr/fe/</a></p>
</div>
