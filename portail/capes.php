<h2 id="capes-informatique">CAPES informatique</h2>
<p>L’université de Lille a ouvert en septembre 2019 une préparation au
CAPES informatique sous la forme d’un parcours informatique du master
MEÉF.</p>
<ul>
<li><a
href="https://gitlab-fil.univ-lille.fr/capes/portail/blob/master/Readme.md">Le
portail du parcours informatique</a></li>
<li><a href="https://www.univ-lille.fr/formations/fr-00083105/">La page
du parcours sur le site de l’université</a></li>
</ul>
<p>Contacts</p>
<ul>
<li>Benoit Papegay et Philippe Marquet</li>
<li><a href="mailto:capes-info@univ-lille.fr">📧
capes-info@univ-lille.fr</a></li>
</ul>
<div class="signature">
<p>Benoit PAPEGAY et Philippe MARQUET<br />
<a
href="http://portail.fil.univ-lille.fr/fe/capes-info">portail.fil.univ-lille.fr/fe/capes-info</a></p>
</div>
