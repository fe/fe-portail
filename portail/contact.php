<h2 id="contact">Contact</h2>
<p>Vous pouvez contacter :</p>
<ul>
<li><a href="https://www.cristal.univ-lille.fr/profil/marquet">Philippe Marquet</a><br />
<a href="mailto:philippe.marquet@univ-lille.fr">philippe.marquet@univ-lille.fr</a></li>
</ul>
<div class="signature">
<p>Philippe MARQUET<br />
<a href="http://portail.fil.univ-lille1.fr/fe/">portail.fil.univ-lille1.fr/fe/</a></p>
</div>
