<h2 id="diu-enseigner-linformatique-au-lycée">DIU Enseigner l’informatique au lycée</h2>
<p>Le DIU <em>Enseigner l’informatique au lycée</em> est proposé dans l’académie de Lille par l’université de Lille.</p>
<p>Le DIU est une formation visant à accompagner des futur·e·s enseignant·e·s d’informatique dans l’acquisition des connaissances et compétences nécessaires à l’enseignement de la nouvelle spécialité NSI, <em>Numérique et sciences informatiques</em> en classes de 1re et de Terminale.</p>
<p>Le DIU est un diplôme de formation continue destiné <em>exclusivement</em> aux enseignants inscrits par les services académiques.</p>
<p>Le contenu de la formation est présenté dans le <a href="https://sourcesup.renater.fr/diu-eil/medias/diu-eil-habilit-2-ppn.pdf">Programme pédagogique national du DIU EIL</a> (PDF, 7 pages).</p>
<p><b>Consultez la page <a href="https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md">gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md</a> pour d’autres informations pratiques</b> (calendrier, lieu de formation, groupes…) et pédagogiques (ressources…).</p>
<ul>
<li>accès directs <a href="https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/calendrier.md">calendrier</a> / <a href="https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/groupes/readme.md">groupes</a> / <a href="https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc1/Readme.md">bloc1</a> / <a href="https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/Readme.md">bloc2</a> / <a href="https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc3/Readme.md">bloc3</a></li>
</ul>
<div class="signature">
<p>Benoît PAPEGAY et Philippe MARQUET<br />
<a href="diu-eil@univ-lille.fr">mailto:diu-eil@univ-lille.fr</a>   <a href="http://portail.fil.univ-lille1.fr/fe/diu-eil">portail.fil.univ-lille1.fr/fe/diu-eil</a></p>
</div>
