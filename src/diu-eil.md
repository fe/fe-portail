DIU Enseigner l'informatique au lycée
-------------------------------------

Le DIU _Enseigner l'informatique au lycée_ est proposé dans l'académie
de Lille par l'université de Lille.

Le DIU est une formation visant à accompagner des futur·e·s
enseignant·e·s d’informatique dans l’acquisition des connaissances et
compétences nécessaires à l’enseignement de la nouvelle spécialité
NSI, _Numérique et sciences informatiques_ en classes de 1re et de
Terminale.

Le DIU est un diplôme de formation continue destiné *exclusivement*
aux enseignants inscrits par les services académiques. 

Le contenu de la formation est présenté dans le
[Programme pédagogique national du DIU EIL](https://sourcesup.renater.fr/diu-eil/medias/diu-eil-habilit-2-ppn.pdf)
(PDF, 7 pages). 

<b>Consultez la page [gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md)
pour d'autres informations pratiques</b> (calendrier, lieu de
formation, groupes...) et pédagogiques (ressources...).

* accès directs
  [calendrier](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/calendrier.md) / 
  [groupes](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/groupes/readme.md) /
  [bloc1](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc1/Readme.md) /
  [bloc2](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc2/Readme.md) /
  [bloc3](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/bloc3/Readme.md) 

::: signature
Benoît PAPEGAY et Philippe MARQUET \
[mailto:diu-eil@univ-lille.fr](diu-eil@univ-lille.fr) \ 
[portail.fil.univ-lille1.fr/fe/diu-eil](http://portail.fil.univ-lille1.fr/fe/diu-eil)
:::
