Formation des enseignants
-------------------------

L'informatique, ou de l'informatique, est aujourd'hui enseignée de l'école au lycée.

Le département informatique, en partenariat avec les autres
départements informatiques de l'université — IUT, Polytech, etc. —
assure la formation continue, bientôt initiale, d'enseignants du
primaire et du secondaire à l'informatique.


* nous avons assuré la formation des enseignants de la spécialité ISN,
  _Informatique et science du numérique_, de l’académie de Lille en
  2011-13. Puis participé ponctuellement à un nouveau cycle de
  formation en 2018. 
* nous avons mis en place une formation _Algorithmique et
  programmation au lycée — Python_ à destination des enseignants de
  mathématiques à l'occasion de l'aménagement du programme de maths de
  la classe de seconde entré en vigueur à la rentrée 2017.
* nous proposons une journée annuelle _JEIA_ à destination de toutes
  celles et ceux qui enseignent de l'informatique, de l'école
  à l'université. 
* nous proposons le DIU _Enseigner l'informatique au lycée_ pour
  formation des futurs enseignants de la spécialité NSI, _Numérique et
  sciences informatiques_. 
* nous travaillons à la mise en place d'une prépa CAPES informatique
  qui ouvrira dès la rentére 2019.
* ...
  
::: signature
Philippe MARQUET \
[portail.fil.univ-lille1.fr/fe/](http://portail.fil.univ-lille1.fr/fe/)
:::
