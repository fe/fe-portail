CAPES informatique
------------------

L'université de Lille a ouvert en septembre 2019 une préparation au CAPES
informatique sous la forme d'un parcours informatique du master MEÉF.

* [Le portail du parcours informatique](https://gitlab-fil.univ-lille.fr/capes/portail/blob/master/Readme.md)
* [La page du parcours sur le site de l'université](https://www.univ-lille.fr/formations/fr-00083105/)

Contacts

* Benoit Papegay et Philippe Marquet
* [📧 capes-info@univ-lille.fr](mailto:capes-info@univ-lille.fr) 


::: signature
Benoit PAPEGAY et Philippe MARQUET \
[portail.fil.univ-lille.fr/fe/capes-info](http://portail.fil.univ-lille.fr/fe/capes-info)
:::
