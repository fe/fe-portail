Contact
-------

Vous pouvez contacter :

* [Philippe Marquet](https://www.cristal.univ-lille.fr/profil/marquet) \
	[philippe.marquet@univ-lille.fr](mailto:philippe.marquet@univ-lille.fr)


::: signature
Philippe MARQUET \
[portail.fil.univ-lille1.fr/fe/](http://portail.fil.univ-lille1.fr/fe/)
:::
