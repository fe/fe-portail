Pages "formations enseignants" du portail
=========================================

http://portail.fil.univ-lille1.fr/fe/

* `src/` les fichiers sources, Markdown
* `portail/` les fichiers exposés sur le portail
  - fichiers générés depuis les `../src/*`
  - fichiers médias dans `portail/medias/`

  
* `Makefile` pour contruire `portail/` à partir de `src/` 
  - les fichers générés doivent être poussés sur le git
  - le portail inclut les fichiers de la branche `master` du dépôt
	https://gitlab-fil.univ-lille.fr/fe/fe-portail/raw/master/portail/





